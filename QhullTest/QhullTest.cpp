// QhullTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include "StlFile.h"
#include "ConvexHull.h"

#include <libqhullcpp\Qhull.h>
#include <libqhullcpp\QhullError.h>

int main(int argc, char* argv[])
{
	std::string outputFile("Output.stl");

	if (argc < 1) {
		std::cerr << "QhullTest INPUT_STL_FILE" << std::endl;
		return 1;
	}

	StlFile stl(argv[1]);

	ConvexHull convex;
	convex.setVertexes(stl.vertexes());
	try {
		convex.execute();
	}
	catch(orgQhull::QhullError &e){
		std::cerr << e.what() << std::endl;
	}

	StlFile outStl;
	outStl.setVertexes(convex.convexVertexes());
	outStl.setNormals(convex.convexNormals());
	outStl.write(outputFile.c_str());
	
    return 0;
}

